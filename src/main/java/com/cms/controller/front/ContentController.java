/*
 * 
 * 
 * 
 */
package com.cms.controller.front;

import com.cms.entity.Category;
import com.cms.entity.Content;
import com.cms.routes.RouteMapping;

/**
 * Controller - 内容
 * 
 * 
 * 
 */
@RouteMapping(url = "/content")
public class ContentController extends BaseController {

	/**
	 * 内容
	 */
	public void index() {
		Integer id = getParaToInt("id");
		Content content = new Content().dao().findById(id);
		setAttr("currentContent", content);
		Category category = content.getCategory();
		setAttr("currentCategory", category);
		render("/templates/"+getCurrentTemplate()+"/"+category.getDetailTemplate());
	}
	
	
   /**
     * 搜索
     */
    public void search() {
        String keyword = getPara("keyword");
        Integer pageNumber = getParaToInt("pageNumber");
        if(pageNumber==null){
			pageNumber = 1;
		}
		setAttr("pageNumber", pageNumber);
        setAttr("keyword", keyword);
        render("/templates/"+getCurrentTemplate()+"/search.html");
    }

}