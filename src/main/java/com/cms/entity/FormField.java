package com.cms.entity;

import java.util.List;

import com.cms.entity.base.BaseFormField;
import com.cms.util.DbUtils;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;

/**
 * Entity - 表单字段
 * 
 * 
 * 
 */
@SuppressWarnings("serial")
public class FormField extends BaseFormField<FormField> {
	
    /**
     * 表单
     */
    private Form form;
    
    
	/**
	 * 获取表单
	 * 
	 * @return 表单
	 */
	public Form getForm(){
	    if(form == null){
	    	form = new Form().dao().findById(getFormId());
	    }
		return form;
	}
	
   /**
     * 判断名称是否存在
     * 
     * @param name
     *            名称
     * @return 名称是否存在
     */
    public boolean nameExists(Integer formId,String name) {
        if (StrKit.isBlank(name)) {
            return false;
        }
        Integer count = Db.queryInt("select count(1) from cms_form_field where formId = ? and name = ?",formId,name);
        return count > 0;
    }
	
	/**
	 * 查找表单字段
	 * 
	 * @param formId
	 * 				表单ID
	 * @return 表单字段
	 */
	public List<FormField> findList(Integer formId){
	    String filterSql = "";
		if(formId != null){
		    filterSql+= " and formId = "+formId;
		}
		String orderBySql = DbUtils.getOrderBySql("sort desc,createDate desc");
		return find("select * from cms_form_field where 1=1 "+filterSql+orderBySql);
	}
	
	/**
	 * 根据表单ID删除表单字段
	 * 
	 * @param formId
	 */
	public void deleteByFormId(Integer formId){
	    Db.update("delete from cms_form_field where formId = ?",formId);
	}
}
