package com.cms;

import java.io.Serializable;

public class Config implements Serializable {

	private static final long serialVersionUID = 1012463140749529805L;

	/** 缓存名称 */
	public static final String CACHE_NAME = "config";

	/** 是否水印开启 */
	private Boolean isWatermarkEnabled;
	
	/** 水印图片 */
	private String watermarkImage;
	
	/** 水印位置 */
	private String watermarkPosition;
	
	/** 是否开启缓存 */
	private Boolean isCacheEnabled;
	

	public Boolean getIsWatermarkEnabled() {
		return isWatermarkEnabled;
	}

	public void setIsWatermarkEnabled(Boolean isWatermarkEnabled) {
		this.isWatermarkEnabled = isWatermarkEnabled;
	}

	public String getWatermarkImage() {
		return watermarkImage;
	}

	public void setWatermarkImage(String watermarkImage) {
		this.watermarkImage = watermarkImage;
	}

	public String getWatermarkPosition() {
		return watermarkPosition;
	}

	public void setWatermarkPosition(String watermarkPosition) {
		this.watermarkPosition = watermarkPosition;
	}

	public Boolean getIsCacheEnabled() {
		return isCacheEnabled;
	}

	public void setIsCacheEnabled(Boolean isCacheEnabled) {
		this.isCacheEnabled = isCacheEnabled;
	}
}
